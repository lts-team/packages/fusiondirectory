# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR FusionDirectory Project
# This file is distributed under the same license as the FusionDirectory package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
# 
# Translators:
# fusiondirectory <contact@fusiondirectory.org>, 2017
# 
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: FusionDirectory VERSION\n"
"Report-Msgid-Bugs-To: bugs@fusiondirectory.org\n"
"POT-Creation-Date: 2018-09-03 11:55+0000\n"
"PO-Revision-Date: 2017-11-03 10:37+0000\n"
"Last-Translator: fusiondirectory <contact@fusiondirectory.org>, 2017\n"
"Language-Team: Finnish (Finland) (https://www.transifex.com/fusiondirectory/teams/12202/fi_FI/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: fi_FI\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#: admin/systems/weblink/class_webLink.inc:31
#: admin/systems/weblink/class_webLink.inc:44
msgid "Web link"
msgstr ""

#: admin/systems/weblink/class_webLink.inc:32
msgid "Edit web link"
msgstr ""

#: admin/systems/weblink/class_webLink.inc:47
msgid "Protocol"
msgstr "Protokolla"

#: admin/systems/weblink/class_webLink.inc:47
msgid "Protocol to use to access this computer Web page"
msgstr ""

#: admin/systems/weblink/class_webLink.inc:52
msgid "Links"
msgstr ""

#: admin/systems/weblink/class_webLink.inc:52
msgid "Web links to this computer"
msgstr ""
