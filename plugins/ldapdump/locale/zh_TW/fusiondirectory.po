# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR FusionDirectory Project
# This file is distributed under the same license as the FusionDirectory package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
# 
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: FusionDirectory VERSION\n"
"Report-Msgid-Bugs-To: bugs@fusiondirectory.org\n"
"POT-Creation-Date: 2018-09-03 11:55+0000\n"
"PO-Revision-Date: 2017-11-03 10:28+0000\n"
"Language-Team: Chinese (Taiwan) (https://www.transifex.com/fusiondirectory/teams/12202/zh_TW/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: zh_TW\n"
"Plural-Forms: nplurals=1; plural=0;\n"

#: addons/ldapdump/class_ldapDump.inc:26
msgid "LDAP"
msgstr ""

#: addons/ldapdump/class_ldapDump.inc:27 addons/ldapdump/class_ldapDump.inc:39
msgid "LDAP Dump"
msgstr ""

#: addons/ldapdump/ldapdump.tpl.c:2
msgid "Insufficient rights"
msgstr ""
