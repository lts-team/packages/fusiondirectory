# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR FusionDirectory Project
# This file is distributed under the same license as the FusionDirectory package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
# 
# Translators:
# fusiondirectory <contact@fusiondirectory.org>, 2017
# 
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: FusionDirectory VERSION\n"
"Report-Msgid-Bugs-To: bugs@fusiondirectory.org\n"
"POT-Creation-Date: 2018-09-03 11:55+0000\n"
"PO-Revision-Date: 2017-11-03 10:26+0000\n"
"Last-Translator: fusiondirectory <contact@fusiondirectory.org>, 2017\n"
"Language-Team: Czech (Czech Republic) (https://www.transifex.com/fusiondirectory/teams/12202/cs_CZ/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: cs_CZ\n"
"Plural-Forms: nplurals=4; plural=(n == 1 && n % 1 == 0) ? 0 : (n >= 2 && n <= 4 && n % 1 == 0) ? 1: (n % 1 != 0 ) ? 2 : 3;\n"

#: admin/ejbca/class_ejbcaManagement.inc:32
#: admin/ejbca/certificates/class_ejbcaCertificates.inc:29
msgid "EJBCA"
msgstr "EJBCA"

#: admin/ejbca/class_ejbcaManagement.inc:33
msgid "EJBCA certificates management"
msgstr "Správa EJBCA certifikátů"

#: admin/ejbca/class_ejbcaManagement.inc:52
msgid "Download"
msgstr "stažení"

#: admin/ejbca/certificates/class_ejbcaCertificates.inc:30
msgid "Assign EJBCA certificates to a user"
msgstr "Přiřadit uživateli EJBCA certifikáty"

#: admin/ejbca/certificates/class_ejbcaCertificates.inc:48
msgid "EJBCA certs"
msgstr "Certifikáty EJBCA"

#: admin/ejbca/certificates/class_ejbcaCertificates.inc:52
msgid "Certificates associated to this object"
msgstr "Certifikáty související s tímto objektem"

#: admin/ejbca/class_ejbcaCertificate.inc:30
#: admin/ejbca/class_ejbcaCertificate.inc:31
#: admin/ejbca/class_ejbcaCertificate.inc:33
msgid "EJBCA certificate"
msgstr "EJBCA certifikát"

#: admin/ejbca/class_ejbcaCertificate.inc:47
#: admin/ejbca/class_ejbcaCertificate.inc:55
msgid "Certificate"
msgstr "Certifikát"

#: admin/ejbca/class_ejbcaCertificate.inc:51
msgid "Name"
msgstr "název"

#: admin/ejbca/class_ejbcaCertificate.inc:51
msgid "Common name"
msgstr "běžné jméno (CN)"

#: admin/ejbca/class_ejbcaCertificate.inc:55
msgid "Certificate content"
msgstr "Obsah certifikátu"

#: config/ejbca/class_ejbcaConfig.inc:28
msgid "EJBCA plugin configuration"
msgstr "Nastavení zásuvného modulu EJBCA"

#: config/ejbca/class_ejbcaConfig.inc:29
msgid "FusionDirectory ejbca plugin configuration"
msgstr "Nastavení zásuvného modulu ejbca pro FusionDirectory"

#: config/ejbca/class_ejbcaConfig.inc:42
msgid "EJBCA plugin"
msgstr "Zásuvný modul EJBCA"

#: config/ejbca/class_ejbcaConfig.inc:45
msgid "EJBCA RDN"
msgstr "Relativní rozlišený název pro EJBCA"

#: config/ejbca/class_ejbcaConfig.inc:45
msgid "Branch in which ejbca certificates are stored"
msgstr "Větev, ve které jsou ukládány ejbca certifikáty"
